﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryCard : MonoBehaviour
{

    [SerializeField] private GameObject cardBack;
    [SerializeField] private Sprite image;

    [SerializeField] private SceneController controller;

    private int _id;
    public int id
    {
        get { return _id; } //добавленная функция чтения
    }

    public void SetCard(int id, Sprite image) //открытый метод которым могут пользоваться  другие сценарии для передачи новых спрайтов
    {
        _id = id;
        GetComponent<SpriteRenderer>().sprite = image;
    }

    public void OnMouseDown()
    {
        //Debug.Log("testing 1 2 3");
        
        if (cardBack.activeSelf)
        {
            cardBack.SetActive(false);
            controller.CardRevealed(this);
        }
        
    }

    public void Unreavel()
    {
        cardBack.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<SpriteRenderer>().sprite = image;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
